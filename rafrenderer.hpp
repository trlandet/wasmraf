/*
 * Render Fujifilm RAF files to SDL
 */
#ifndef RAFRENDERER_H
#define RAFRENDERER_H
#include <cstdint>
#include <vector>
#include <string>

struct RafFile {
    int width, height;
    std::vector<uint16_t> data;
};

void parse_and_render_file(std::string file_name);

#endif // RAFRENDERER_H


