WasmRAF
=======

WasmRAF is a Javascript (Web Assembly / C++ / emscripten) implementation of the
Fujifilm RAW (*.RAF) file format. It contains a simple C++ RAF parser and a
web/desktop display code based on the SDL2 library. In principle it can be
extended to support RAF files on the web, just like PNG and JPEG files are 
supported today. As RAF files are rather large and often need post-processing
the use cases for this are probably rather limited.

The current implementation only contains a crude RAF parser (works on X-T1
files and probably more). The demosaic algorithm is basically nonexistent as
of now, it tries to find the average RGB value in each of the 6x6 repeating
XTrans patterns and then shows this downscaled rendering on a web page or in a
separate window if used from the console. Currently the colours are way off
and the overall rendering is rather bleak and dark.

The "developed" RAW image is written to a HTML canvas (via SDL2). The format
parsing and the display code are quite fast when running on bare metal, but
the display code is quite slow in the browser.

This is currently a weekend experiment only, written in a few hours to learn 
Web Assembly and the RAF file format. Please do not expect work to be done with
the code base at regular intervals (or at all) by the author. Also, I have not
worked with Javascript in the last 10 years, so the quality of the code may not
be the best or follow best practices, but the whole thing seems to work for me,
and if you can use it to learn something, then good for you!

You can build the console and html applications with CMake, or just run 
emscripten directly on the ``rafrenderer.cpp`` file:

::

  emcc rafrenderer.cpp -o rafrenderer.js -O2 -s USE_SDL=2 -s ALLOW_MEMORY_GROWTH=1 -std=c++14

Open index.html and try to drag and drop a RAF file into the webpage. The
console application currently always tries to read ``example.raf` (all lower
case), but you can alos give the name of a RAF file as the first command line
argument.

Code (c) Tormod Landet, March 2017
