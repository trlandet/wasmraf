/*
 * This is a test application to use the RAF renderer from the command line
 */
#include <string>
#include "rafrenderer.hpp"


using namespace std;


int main(int argc, char *argv[])
{
    string file_name = "example.raf";
    if (argc > 2)
        file_name = argv[1];
    parse_and_render_file(file_name);
    return 0;
}
