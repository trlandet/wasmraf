#include <cstdint>
#include <cassert>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <SDL2/SDL.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#else
#define EMSCRIPTEN_KEEPALIVE
#endif

#include "rafrenderer.hpp"

using namespace std;


// Swap byte order from big to little endian
template <typename T>
T bswap(T val) {
    T retVal;
    char *pVal = (char*) &val;
    char *pRetVal = (char*)&retVal;
    int size = sizeof(T);
    for(int i=0; i<size; i++) {
        pRetVal[size-1-i] = pVal[i];
    }

    return retVal;
}


string read_string(ifstream &file, int length, bool zero_padded=false) {
    string ret(length, '@');
    file.read(&ret[0], length);
    assert(!file.fail());

    if (zero_padded) {
        auto pos = ret.find('\0');
        if (pos != string::npos)
            ret.resize(pos);
    }

    return ret;
}

template<typename T>
T read_single(ifstream &file) {
    T ret;
    file.read(reinterpret_cast<char*>(&ret), sizeof(T));
    assert(!file.fail());
    return bswap(ret);
}


/*
 * Read a Fujifilm RAF file. Based on documentation from
 * https://libopenraw.freedesktop.org/wiki/Fuji_RAF/
 */
RafFile read_raf(string filename, bool verbose=false) {
    ifstream file;
    file.open(filename, ios::in | ios::binary);
    if (!file) throw "Cannot open file!";

    // Header
    auto magic = read_string(file, 16);
    auto head2 = read_string(file, 4);
    auto head3 = read_string(file, 8);
    auto camera = read_string(file, 32, true);
    if (verbose) {
        cout << "File magic: '" << magic << "'" << endl;
        cout << "File head2: '" << head2 << "'" << endl;
        cout << "File head3: '" << head3 << "'" << endl;
        cout << "Camera: '" << camera << "'" << endl;
    }

    // Offset table
    auto version = read_string(file, 4);
    read_string(file, 20);
    auto jpeg_offset = read_single<uint32_t>(file);
    auto jpeg_length = read_single<uint32_t>(file);
    auto cfa_head_offset = read_single<uint32_t>(file);
    auto cfa_head_length = read_single<uint32_t>(file);
    auto cfa_offset = read_single<uint32_t>(file);
    auto cfa_length = read_single<uint32_t>(file);
    if (verbose) {
        cout << "Offset version: '" << version << "'" << endl;
        cout << "Jpeg offset " << jpeg_offset << " and length " << jpeg_length << endl;
        cout << "CFA header offset " << cfa_head_offset << " and length " << cfa_head_length << endl;
        cout << "CFA offset " << cfa_offset << " length " << cfa_length << endl;
    }

    // Constants from libraw
    int16_t RAF_TAG_SENSOR_DIMENSION = 0x100;

    // CFA header
    file.seekg(cfa_head_offset);
    auto num_items = read_single<uint32_t>(file);
    uint16_t w(0), h(0);
    for (int i = 0; i < num_items; i++) {
        auto type = read_single<uint16_t>(file);
        auto size = read_single<uint16_t>(file);
        if (verbose) {
            cout << "CFA header tag " << type << " size " << size;
            if (size == 4) {
                 cout << " value " << read_single<uint32_t>(file);
                 file.unget(); file.unget(); file.unget(); file.unget();
            }
            cout << endl;
        }
        if (type == RAF_TAG_SENSOR_DIMENSION) {
            assert(size == 4);
            auto dims = read_single<uint32_t>(file);
            h = (dims & 0xFFFF0000) >> 16;
            w = (dims & 0x0000FFFF);
        }
        else {
            read_string(file, size);
        }
    }
    if (verbose) {
        cout << "CFA num headers " << num_items << endl;
        cout << "Width " << w << " height " << h << endl;
        cout << "LENGTH1 " << cfa_length << " LENGTH2 " << w*h*2 << endl;
    }
    assert (w > 0 && h > 0);
    assert(cfa_length - 2048 == w*h*2);

    // Read the raw data
    auto raf = RafFile();
    raf.width = w;
    raf.height = h;
    raf.data.resize(w*h, 0);
    file.seekg(cfa_offset + 2048);
    file.read(reinterpret_cast<char*>(&raf.data[0]), w*h*2);
    if (verbose) {
        cout << "RAF data center " << raf.data[w*h/2] << " max " << UINT16_MAX << endl;
    }

    return raf;
}


void render_raf(RafFile &raf){
    if(SDL_Init( SDL_INIT_VIDEO ) < 0) {
        cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
    }
    SDL_Window *window;
    SDL_Renderer *renderer;
    int width = raf.width/6, height = raf.height/6;
    SDL_CreateWindowAndRenderer(width, height, 0, &window, &renderer);

    // Red background
    SDL_SetRenderDrawColor(renderer, 255, 100, 100, 255);
    SDL_RenderClear(renderer);

    // Linear -> sRGB color, see http://entropymine.com/imageworsener/srgbformula/
    auto to_srgb = [](double L) {
        if (L < 0.0031308) return L*12.92;
        else return 1.055 * pow(L, 1.0/2.4) - 0.055;
    };

    // Display the RAF data
    //  1) Demosaic
    //  2) "Develop"
    //  3) Convert to 8bit sRGBA
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            // XTrans red pixels
            uint32_t r = (uint) raf.data[i*6 + 2 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 5 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 5 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 2 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 0 + (j*6 + 5)*raf.width] +
                         (uint) raf.data[i*6 + 4 + (j*6 + 5)*raf.width];
            // XTrans green pixels
            uint32_t g = (uint) raf.data[i*6 + 0 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 4 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 0 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 2 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 5 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 0 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 4 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 0 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 4 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 2 + (j*6 + 5)*raf.width] +
                         (uint) raf.data[i*6 + 5 + (j*6 + 5)*raf.width];
            // XTrans blue pixels
            uint32_t b = (uint) raf.data[i*6 + 5 + (j*6 + 0)*raf.width] +
                         (uint) raf.data[i*6 + 2 + (j*6 + 1)*raf.width] +
                         (uint) raf.data[i*6 + 0 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 4 + (j*6 + 2)*raf.width] +
                         (uint) raf.data[i*6 + 2 + (j*6 + 3)*raf.width] +
                         (uint) raf.data[i*6 + 5 + (j*6 + 4)*raf.width] +
                         (uint) raf.data[i*6 + 1 + (j*6 + 5)*raf.width] +
                         (uint) raf.data[i*6 + 3 + (j*6 + 5)*raf.width];

            // Convert averages to values in [0, 1]
            double rd = r / (pow(2.0, 14) * 8);
            double gd = g / (pow(2.0, 14) * 20);
            double bd = b / (pow(2.0, 14) * 8);

            // Apply sRGB gamma curve
            uint8_t r8 =  (uint8_t) (to_srgb(rd) * UINT8_MAX);
            uint8_t g8 =  (uint8_t) (to_srgb(gd) * UINT8_MAX);
            uint8_t b8 =  (uint8_t) (to_srgb(bd) * UINT8_MAX);

            SDL_SetRenderDrawColor(renderer, r8, g8, b8, 255);
            SDL_RenderDrawPoint(renderer, i, j);
        }
    }

    // Show the results
    SDL_RenderPresent(renderer);

#ifndef __EMSCRIPTEN__
    // A small main loop in case we are running as stand alone program and not inside a webpage
    SDL_Event event;
    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
#endif
}

void parse_and_render_file(string file_name) {
    clock_t begin = clock();
    auto raf = read_raf(file_name, true);
    cout << "Rad and parsed RAF in " << double(clock() - begin) / CLOCKS_PER_SEC << " seconds" << endl;

    begin = clock();
    render_raf(raf);
    cout << "Rendered RAF in " << double(clock() - begin) / CLOCKS_PER_SEC << " seconds" << endl;
}

extern "C" {
EMSCRIPTEN_KEEPALIVE
void render_example_file() {
    parse_and_render_file("example.raf");
}
}
